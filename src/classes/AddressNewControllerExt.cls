/*************************************************************************************************************
** User Story: 
**************************************************************************************************************
** Class Name    : AddressNewControllerExt
** Description   : Controller for components that add parcel objects to contacts
** Version       : 1.0
** Built By      : 
**------------------------------------------------------------------------------------------------------------
** Modification Log:
**------------------
** Developer                         Date                    Version                      Description
**------------------------------------------------------------------------------------------------------------
** Hannah Skurnik                  11/10/15                                             Added overloaded saveLocalAddress method
** Okwudiafor Akosa                11/28/17                                             Created comment logs for the class.
** Review Log:
**---------------
** Reviewer                  Date           Version               Description
**------------------------------------------------------------------------------------------------------------
**
**  **********************************************************************************/
public class AddressNewControllerExt
{
   
}