/*************************************************************************************************************
** User Story: Ability to generate a new security code(7digit) on Account object
**************************************************************************************************************
** Class Name    : AccountSecuirtycodeGeneration
** Description   : Used to send secuirty code to asscocite contacts with authorized representatives
when users click on generate new secuirty code Button
** Version       : 1.0
** Built By      : Siva Pamidi <spamidi@deloitte.com>
**------------------------------------------------------------------------------------------------------------
** Modification Log:
**------------------
** Developer                         Date                    Version                      Description
** Siva Pamidi                      10/24/2017                1.0                       Initial Creation v1.0
** Srikanth Kottam		 			02/12/2018	   			  2.0		     Added exception handling and Logging Errors
**------------------------------------------------------------------------------------------------------------
** Review Log:
**---------------
** Reviewer                  Date           Version               Description
**------------------------------------------------------------------------------------------------------------
**
**  **********************************************************************************************************/

global with sharing class AccountSecuirtycodeGeneration {
   
}