/*************************************************************************************************************
** User Story: 
**************************************************************************************************************
** Class Name    : AddressForAccountController
** Description   : Creates new addresses and junction objects between either contact or account
** Version       : 1.0
** Built By      : Hannah Skurnik 
**------------------------------------------------------------------------------------------------------------
** Modification Log:
**------------------
** Developer                         Date                    Version                      Description
**------------------------------------------------------------------------------------------------------------
** Hannah Skurnik                10/12/2015                      1                        Created
** Aishwaria Rangineni			 02/21/2018						 1.1					  Added error logs and deleted debug statements
** Review Log:
**---------------
** Reviewer                  Date           Version               Description
**------------------------------------------------------------------------------------------------------------
**
**  **********************************************************************************/

public class AddressForAccountNewControllerExt
{
}